<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>movieCard</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <link rel="icon" href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAANwAAADlCAMAAAAP8WnWAAABDlBMVEX8blH////gYkhPWmnw8PDv7+//zlT39/f6+vrz8/P8/Pz/b1DmYkZKWmrkYkd9XGBFVGpIVmmzml7/0lOUYWGqX1ZATV5YYGh9d2RjcoDV19r8aUr8Y0Hv9fZKVmX8ZUXfWj31a09CWWreVTb8YD3f4eOHjpd1fYh+hY/xaU3/0cn/9fP4mYhVWmjLztL0xLzRYUzy29f+vrP+rJ78inT+tqr6gGj8emD9kHzzzsj+3Nbx5+X/5eByXWXka1RnXGbBZFi0uL4xTmLHYE6lqrGiXljkg3HptazqmIntrqPnoZb5lYL6rJ6MYGFRUF7WaVe6ZVuqrrVTZ3YzWW2WnKTokIBnaWfqwrvkd2Hlf2u2zoRHAAAQg0lEQVR4nO2da1vbOhKA42BvYpuE7S5kl8bCJinQkkCaEAghlBZ6oRR62nLaPfT//5GVfLdjWSNfEpWe+VI/rmL0WqOZkTSWKhIRRZZllVzUyVWNXNXwhVInVyq58ovJ5ELzi5EfOMXsH2jkSo78YInPrYhQib/hioFTnMIKoxJKvBIKoxKLf64Np8l+YTn8JmS/sOw/XfV/EBQLrjT/ByI8t6Jgcd8aFvd1YHHKkSvn75Mr5615xewfuG/NK6ZGii37uRX5F+tHHM+VfyM4YGf21CfSmV31STMSfrco+LmU+soVlYiGpU4uauSqFr2qkyu/mOb/Zz2hWC1SzLlCWOw/aP/AroR7L9dzAfV1XUFJRgIheTbcHx31x5NK23xmi9GuTE5uj44H06GCULbnAutbkhNXca17Z6P+iW4Yptlu65WI6O122zQNc3J6vN/D70Atp3+WAYeQOhz0JxgrDjUnmPFZ5fR46rZhWXDAzqywO3Nv0N/GXAysCKHRHt+cs56L1ZLT+FTqRGpE/IukqzrlKlqsrs1GY5MHLFBUY3J0Vke5/nz8B4VGKL3RiWGyNDGlBY3K0bmQEQqq75+aOcg8vsloRuyLSBEKmt1VjAzamCCmeTtFqBi4ItRSG95m6Wc0wc03qBeilrW8UtfOxgU1mi+6WRnVtNxVyx2hoOmJkbenJYnZvgn9+eVEKOfjUtBsvMqIq98XHaH0+qWh2XiT/SLgsqglko6LNCNJohun50jOrpaOUUhz+5ToQJpOzHLRiLSNu7rGF6YE/0n8HCBenI/rsEaWj0bEnJzxx7d2fTM78X29ZI0MRDeOJLS4CAXJi2o2R9rbZ2hREQoaVhbWbK4Yd0jOEKHwz19Ix88WjIbFPOlJ3PMtvINVGUmnC1VJT9rts0jHAw1WQ0ZCAjjxJaikJ89ukFxqhIIGuYds2cXoq2VGKOh4KSrpiXni2BJwhCLLHCssR0tlwx1v0kMc9eWZTke3C4i30kXXhwhcXy4nPl46GxbbaMLqC4dTlZNlmcmomFNUdIQiDBs2mlPErq8TocCsjywJw4bpzlDmaYakBXR5LA4b1kxiVaAL/kyniE5FYsNWBdMVlaqB+iLYyZDolRm85XxPnpj6gO4EY8N0EyU9VcOBY1tLabDkuCRJ2mMJYC2Zfg6dCciGjUof5Xfi6mx5w4BUMUYod6oGEsjBRcUYInCqhhYdsbtXwhnKsCiJqRrBjAgrQtkXssM50j7NtRCi9QRuN9LtpOzTDLIkVNQ1L8a5DIJLmk1Cx0I3HPHlKkeqRnR1YShwh3PEvEMpazapEcrJEl2cTuQFs5hxnjFCGS2n4QhV5eLL1/VXBx124ZNsEcrCLaXdWNsXbzY+v250Op1GY6Xzkvkjc5RpmuF2cZbSw3q/9sfVlY3lSOdNk/lbs8efqlE/W8hyh9O1sA6ubV1d3T9/d7jzrrXiS2ej2dxmPKF9S83poEYo9UnJ1sTGuviysf7K1sGt7475lqTNLR+u8Xm1yqR7NuSOUMobxOmOxXjz/nNUB2sunBw0XePVapVJR2wKZ4RSGtb2F9y1DjDW83eXO7XnfjNt7blw0mFAd9CssumM/XS4ObUcFWsq7cZ64Zh30litw7oDogY62Lr06Hb9m52XVQDdhKaWycGJWpildOzgW2zeWyEdTGym1o57rx5YlLek6arV9L9hDOocqRpFBJWBeX/duOrsfb98qtQCO9j65tF99+m2Nr17T7172FxWAXTbCJ6qIcu5Gs4z77hr2TrYWFHjlQ41k3TvK2Hru3fP8weN9dUqgM4cIHCEgrL2uCByIjrY8ezgll/pUDOtePd2QsRPvZuuP7DNJZtOnyTDJUUoKBNVEDltEh1MrXSE+FtQcEtz7ymtkLl0JM2qGFNoqkadZ24h6FoHTkC4tZtQ6ZV6rNKE+DCB+Ll379Ip2PDZUm2mPtaAqRraGBaceJET1sGre18HQ5Xeo1favul1xZDbDvzB3lYDv60/3zZBdMZQThisenCBE1eBY9QXG+u2Du5eHu5oyZV+klhpv1yiP8D6fPhtd/Oqs/L68/s3L6tVUNu1jxAoQkEfYbbyxZ/YDgaVZsQaLS90rKW6bdxaV1d/vFrfePuyubrabFarUDpQhCIrQD+gH3BUOuTEkty2skWwOo3Xa+tf31YTsJh05kCej1Ds2UtJ0yRNVlXsCjRpH+gH9LUGs9L1lcCJvfMKht22pjy9/L7ZuVp5vUZ0cJWKxfII+hhXvUYYyHd5Tp5YwnQ6dKFRX29QK33v3Ut02551JI111Xr1ecPGajKwGHRGD5CqAZ5d0L92YpVOizVCbrt22bJ18IDWtTLRmSNAhAKOTvQvHbfSnudlue1dFevgXuOqhXUwtWtloSPDOmaEAnRyWF50vEp7IOFYw3fbER30uhZQB5Ml2apgvUxO1fCduDzjCCsPvGZKcdv1nd0nJHQ5sLtW1saKSrLNNEeI4cTlARxOf+VHxp7bVsOxxs7lu+fYax2s4a4FsIN56fQxYjhx1IePdlxzuZLgthvEGbf+wF0LYt4LojNnDDgZjBaYy5XAbe88vSc66EROzZxdi5fOHGipcBrP2odnLp1ud/hu9z4lcipc5unaR1IMLmot+YapFx1fBztYBwvvWuky/7InKHUhBPHNodtk2A66kZOthoULnM6YpTtxHrSK/hpHuu83vn7dKFHewunMgZoGx7fcSELnRuN+05H7ji/unc1Ow7uTtVCn408RJUis37XvUEqqBuLwci5cMMgOgi8/2vRHPL6vCAbdfoD23S/kDTBC3jI0/8Wmw54uLVXjiKvLOYMeH+X5HMrOPMruHEowreJHOt+3gHAxj6Dbr4a2EAIPLAM4GMrePAqdN7wWkgoXoyOLyHQ/x7ds5cBlbroE3vmmY8BF6cz9ZDhbLXt861YuHF+v41RVFlyErn0sRQJnZ7ayVrPnLTmzD124AGUvDYXOm6aqTLiwVWn3JTJviWnsGcxIhMI7je7BpTVdXisDgAv8nX6CqBEKuuNbAPHgkpquKF4IXIgOUZ04Z/AVwPlj8QQUEC9dVUFwPp05o00zyIgzZciHS0JJ4eVpOhic1++MoTyXquF9R86FFobzUZ5mbDoqLxDOpTOn9RBQNELhXJYL4IKJZ86mS+CNNh0UzvEIxNFRIhRONxeG29p96si3YO7y0Lmzs+kXer7j3LpMK7TnFWrxwDl0ZPKSEqHww634stVyJIgKV9w7rYyFPLWEjn0JHfHilIWQc97EGv3F58ZKqXLAM67ftmcaklI18D9T7qXwYAKsLOGbaW/3w0kbkVQNEeF42KrV9i2ipGpwDlVFhGueIkqE8hjgPiBahPKY4OZSNR4DnERL1YAuGAsNR1vleSRwyRHKI4D7QY1Q+L9xjMP9swDJDTefqkGsSm4n/p9/FSD/ywFnfUS2LZlP1YDmRaXA/bsA+e+THHCfaE5cnuWH+0duyQd3Q4VTcqvl0uGuaRGKwp/9KxzcX3JSqoY9nT751eG6Q/pawSnvJy7Cwc3ocBx5GoLCaXS4418d7kKiw3HHX4LBkdCSlqrBuSQuHhzx4dRUDW5HJxrcNX0hREa8vkAwuO5UTYHj/U5VNLhZQqqG5KVq8JpLweAuvFQNKSlVg3dEJxYcGc2lpGpwrxYIBWddSyl+jju6FAuuO6TAOWopAT91ERPOcr8zi6VqeJ+Y8U5dCgVHJi0j+wLHkklrvGk2IsFZ18Enx8m7avCt+QsF1z1nfRHC5+mEgqsmfe4S+ZSab3VVJDjr49yn1PFd/zUeNqHgulMtdnLB3MeBPF9NiAW3yt5Vg2/aWSC45g/E/maV6yNxgeCsqcr+ZhXx5DkLBFcNPqWOpWoEu/PU6jz2Uhw465MWwbANSixCIVccflwcuC7J3WZFKFh4PqETBa75QGoO2FWjluUTuiXDWYMkuIRdNTT4uEccOOquGn52unNVn4G3/BIFzrrR/Jz0UHZ6dLDqBpjgDbfja+JPCpDIA4Fw3R78AJRzaNMJks1AYmYZvO8X9JseQeC6M559v6Ab7YkBZ/2g7PuVvNUxgm5mIwRcd0bZ6ji6EOLtb6lOYTGYEHDWD8R3AArwhAIh4HDD8e1MKsPCZxHgrE/+9kPQnUlhX3iKANe0LUjKASjy/GEMs18ErnvtN5rM9nPups+gj+mWD9d8QFkOQIF8k6Wvk40CSxQmXHeoZjkABZLEp2+slSwMNmJN2C2XsHc65OATvbJarjCU8kJJ2zudZi3tlwDZEXibpThlir0il/EAFFBeyhLprE/R+nIdgALbfnVpdM2LXAegINBpDMui6w4hB6DYK5LuXnvujnXOFfDgmuXQda9RvL72Xnu0VI35A0VgI7tl0JFRXN6T4GEHdL5YOFvzIrm+fEe0wVKmFk5n9eBwKcfpTkSkc3JOGMf/Ag5CnlVAVmWhdHgsADi4OTVCcT3+OWzxYIFWpXsDOtAy1Yl7ThGY77YwOutjkeesQretXhDbD1Z9KakaFI8PPHhiIW1nfWDXN5aqET1MKb7/vwY8pGcBdNZPQH3TD0CZ8/gj2CR06XTWT/iR22wn7jnFAYyuZI9g6ySovnwnwe8D6crcAtK2JaWcBA+cYy+x7bofeepLPVEpSbTzCigSK63fYd/NU1/okduy8zp6sDizJLruwGk0aH15ToK32xp21H0ZdM3m0O1uhZ8E7/fSO1jHK5zNeuhJvHB8aokFTdvLmFfpfpR9WwJWy7QzH2O3nFxNTeuNFz4n1rSmkvvn4fWtQwar/uBP8eJQ0NRDgR7B+tmTnHhR4akvjxMP9F0abgNUsyi6JvYA0X5fQoQS6syyevSMPTwvJlaxHoZIywMH8/ihziyj4YTd8wpoO9xsSI4bNWiEwjiVOpL6ECkmaccmUzfzWpVm98NMYlQk5VTq9Ol0Zd7P+SqhodmtwdLNfHTWxRTFdrJX4PXN4MQjyd7DMRMvB1rzWp3LOC83Qol2Zu3shIWXFc26UROOgCo3QpnrzPsMvEyaaa3e9FhGjR2hSB5nPFWDXPlvTfaLua4gVAyh6dhIMy3cdE2reqMgNVQRLfLnofVNWfAHqwT+0+e3aXh8HqHZfRjYrw/YNej1zejE5/W9d1yh88G9edPq/him9KO8EQp3y7lLzGjaN2ieD0aHyR6unSNaOOCYLccdoSR1Zhkp+7dmMh+73xGymyFCchajRolQ8lvL8IMRUgb9SlIDptJhMOvnNSbL/YLzTTMw9R3X8Hx0quMWjDkIKphlPXya9hA2jzn7fdFOnFqJ4ag/aeMmDCHGqTBW17r4cDPtcTw3OxxXbBnrzLEYELcgUs73j/sn223TMHFDttuWL92uVX348WlwNlNxL+N5Lnds6R5gFvGHMQfqe0aN7UD9pzubBqiz4XA6GB0fH919wnJzcz34azqcOUiqmvW5gPoWaS3TjA9uR+eMMfuquOemW8t5lShE37n6Z1nP/T3ggLNf3MZnmc+FpGrMD/HrtCF+nTITsJznFhyhlG8klhuhiNM/y4tQRIGDpGpwT2Ury38uOFWDXYz7B+U/d1ERylKe+3s48UcM90jVkpbm8Bjk7wgli74L0D8feYQigvqU9dxKWdGBCM/Nlqrxi8Shv4MT/xvul4P7P8KciWRVmA0XAAAAAElFTkSuQmCC">

    <style>
        *{
            font-family: 'Poppins', sans-serif;
            -webkit-user-select: none;
            -moz-user-select: -moz-none;
            -o-user-select: none;
            user-select: none;
        }
        img {
            -webkit-user-drag: none;
            -moz-user-drag: none;
            -o-user-drag: none;
            user-drag: none;
        }
        img {
            pointer-events: none;
        }
        .movie_card{
            padding: 0 !important;
            width: 22rem;
            margin:14px;
            border-radius: 10px;
            box-shadow: 0 3px 4px 0 rgba(0, 0, 0, 0.2), 0 4px 15px 0 rgba(0, 0, 0, 0.19);
        }
        .movie_card img{
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
            height: 33rem;
        }
        .movie_info{
            color: #5e5c5c;
        }

        .movie_info i{
            font-size: 20px;
        }
        .card-title{
            width: 80%;
            height: 4rem;
        }
        .play_button{
            background-color: #ff3d49;
            position: absolute;
            width: 60px;
            height: 60px;
            border-radius: 50%;
            right: 20px;
            bottom: 111px;
            font-size: 27px;
            padding-left: 21px;
            padding-top: 16px;
            color: #FFFFFF;
            cursor: pointer;
        }

        .credits{
            margin-top: 20px;
            margin-bottom: 20px;
            border-radius: 8px;
            border: 2px solid #8e24aa;
            font-size: 18px;
        }
        .credits .card-body{
            padding: 0;
        }
        .credits p{
            padding-top: 15px;
            padding-left: 18px;
        }
        .credits .card-body i{
            color: #8e24aa;
        }
    </style>
</head>
<body>

</body>
<div class="container mt-5">
    <h2 class="text-center">Movies Library Search</h2>
    <div class="row justify-content-center">
        <div class="card movie_card">
            <div class="card-body">
                <form method="POST" action="{{route('results')}}">
                    @csrf
                    <div class="form-group">
                        <input type="text" name="word" class="form-control" placeholder="Type your movie search word here">
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>

    <div class="row justify-content-center">
        @if(isset($results) && $results['Response'] == 'True')
            @foreach($results['Search'] as $movie)
                <div class="card movie_card">
                    <img src="{{$movie['Poster']}}" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title">{{$movie['Title']}}</h5>
                        <span class="movie_info">{{$movie['Year']}}</span>
                    </div>
                </div>
            @endforeach
        @else
            <h2>No Matched Data.</h2>
        @endif
    </div>
</div>

</html>
