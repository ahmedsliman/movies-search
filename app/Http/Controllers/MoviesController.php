<?php

namespace App\Http\Controllers;

use App\Models\Weather;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class MoviesController extends Controller
{
    public function search()
    {
        return view('index');
    }

    public function results(Request $request)
    {
        $apiUrl = "http://www.omdbapi.com/?apikey=245bd99a&s=".$request->get('word');

        // API Request
        $response = Http::get($apiUrl);
        $results = ($response->ok()) ? $response->collect()->toArray() : [];

        return view('index', compact('results'));
    }
}
