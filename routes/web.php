<?php

use App\Http\Controllers\MoviesController;
use Illuminate\Support\Facades\Route;

Route::get('/', [MoviesController::class, 'search']);
Route::post('/', [MoviesController::class, 'results'])->name('results');
